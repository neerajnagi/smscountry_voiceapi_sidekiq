#!/usr/bin/php
<?php
/**
 * Configuration and simple class for synthesizing text using IVONA SaaS API.
 *
 * Full documentation of IVONA API SOAP is available here: http://www.ivona.com/ivona_tts_saas_api.html
 */

// IVONA SAAS API CLIENT CONFIGURATION 
#define('USER',       'WishThunder@mailinator.com'); // IVONA.com account name (required for IVONA SaaS)
#define('PASSWORD', '5dhs29zmuJO00incfFgJH71RXKEKOnM6'); // IVONA.com account password (required for IVONA SaaS)
define('USER', 'madhusambasivarao.polisetti@smscountry.com');
define('PASSWORD', '8Fezszo0wb185tVMjwgze0tiVOSJOFZQ');


define('SELECTED_VOICE', 'us_eric'); // selected voice to synthesize

class IvonaTextToSpeech {

	/**
	 * Function synthesizing a text 
	 * Returned variable is an url of the file
	 *
	 * @param string $text UTF-8 encoded string to synthesize 
	 * @return string URL address for speech file synthesized from $text parameter, or false in case of error
	 */
	public function synthesize($text, $text_type) {
		// the configuration (could be moved to constants section of the website)
		// wsdl URL
		$wsdl = 'http://www.ivona.com/saasapiwsdl.php';

		// soap client initialization (it requires soap client php extension available)
		$Binding = new SoapClient($wsdl,array('exceptions' => 0));


		// getToken for the next operation
		$input = array('user' => USER);
		$token = $Binding->__soapCall('getToken', $input);
		if (is_soap_fault($token)) {
			error_log('API call: getToken error: '.print_r($token,1));
			return false;
		}

		// additional parameters
		$params = array (
                '0' => array(
                                // the text should be read two times slower
                                'key' => 'Prosody-Rate',
                                'value' => '100'                                         ),
                '1' => array(
                                // 2.5s of pause between paragraphs
                                'key' => 'Prosody-Volume',
                                'value' => 'x-loud'
                        ),
                );
		//$params[]=array('key'=>'Prosody-Rate', 'value'=>PROSODY_RATE); // example value for the new text speed

		// createSpeechFile (store text in IVONA.com system, invoke synthesis and get the link for the file)
		$input = array('token' => $token,
				'md5' => md5(md5(PASSWORD).$token),
				'text' => $text,		
				'contentType' => $text_type,
				'voiceId' => SELECTED_VOICE,
				'codecId' => 'mp3/22050',
				'params' => $params,
			      );
		$fileData = $Binding->__soapCall('createSpeechFile',$input);
		if (is_soap_fault($fileData)) {
			error_log('API call: createSpeechFile error: '.print_r($fileData,1));
			return false;
		}
 
		// return the sound file url
		return $fileData['soundUrl'];
	}

	/**
	 * Function listing all voices 
	 * List of all voices printed on screen
	 */
	public function listVoices() {
		// the configuration (could be moved to constants section of the website)
		// wsdl URL
		$wsdl = 'http://www.ivona.com/saasapiwsdl.php';

		// soap client initialization (it requires soap client php extension available)
		$Binding = new SoapClient($wsdl,array('exceptions' => 0));
		// getToken for the next operation
		$input = array('user' => USER);
		// requesting a new token for an authorized request
		// (single token could be used only once)
		$result = $Binding->__soapCall('getToken',$input);
 
		// checking if there was an error during SOAP request
		if (is_soap_fault($result)) {
			// if there was an error, we should check the faultstring, and correct and prepare another valid request
		        echo "\nSOAP Fault:\n faultcode:[{$result->faultcode}]\n faultstring:[{$result->faultstring}]\n
		                faultactor:[{$result->faultactor}]\n";
		        return;
		}
 
		// the request was successful, so the result is a token
		$token=$result;
 
		// preparing variables for an authorized request (every method and variable is defined in WSDL)
		// @param string $token received token
		// @param string $md5 md5() encoded string
		//      prepared as follows: md5(user password for email test@ivosoftware.com) followed by a token
		$input = array('token'=>$token,'md5'=>md5(md5(PASSWORD).$token));
 
		// requesting user utterances list
		$result = $Binding->__soapCall('listVoices', $input);
 
		// checking if there was an error during SOAP request
		if (is_soap_fault($result)) {
		        // if there was an error, we should check the faultstring, and correct and prepare another valid request
		        echo "\nSOAP Fault:\n faultcode:[{$result->faultcode}]\n faultstring:[{$result->faultstring}]\n
                		faultactor:[{$result->faultactor}]\n";
		        return;
		}
 
		// there wasn't an error, so we've got a valid result, which we could use the way we need
		foreach($result as $k => $v){
		                echo "\n\nLp.".$k;
                		echo "\n\t id = ".$v->voiceId;
		                echo "\n\t name = ".$v->voiceName;
                		echo "\n\t description = ".$v->voiceDescription;
		                echo "\n\t gender = ".$v->gender;
                		echo "\n\t language = ".$v->langId;
		                echo "\n\t productName = ".$v->productName;
                		echo "\n\t providerName = ".$v->providerName;
		}
	}
}

// example command line execution for getting URL for single text
if (!empty($argv[1])) {
	$IvonaTTS = new IvonaTextToSpeech();

	// there wasn't an error, so we've got a valid result, which we could use the way we need
	foreach($argv as $k => $v){
		switch($v) {
			case "--text":
				echo "\n\n".$IvonaTTS->synthesize($argv[$k+1],$argv[$k+2])."\n";
				break;
			case "--list_voices":
				$IvonaTTS->listVoices();
				break;
				
		}
	}
}
?>
