require 'zmq'

context = ZMQ::Context.new
chans   = %w(api)
sub     = context.socket ZMQ::SUB

sub.connect 'tcp://127.0.0.1:5555'
chans.each { |ch| sub.setsockopt ZMQ::SUBSCRIBE, ch }

while line = sub.recv
#  chan, user, msg = line.split ' ', 3
#  puts "##{chan} [#{user}]: #{msg}"
puts line
end
